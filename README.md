1. Clone the repository


2. Run docker by typing
**docker-compose up -d --build**


3. Initialize composer packages by
**docker-compose run --rm php74-service composer install**


4. Create a DB by
**docker-compose run --rm php74-service php bin/console doctrine:database:create**


5. Prepare migration by this commmand, and then hit enter
**docker-compose run --rm php74-service php bin/console  make:migration**


6. After checking migration status, process it by commmand and accept by hitting enter
**docker-compose run --rm php74-service php bin/console doctrine:migrations:migrate**


7. We also need to start yarn dev for encore routing purposes
**docker-compose run --rm node-service yarn install**
&
**docker-compose run --rm node-service yarn dev**

8. Done
